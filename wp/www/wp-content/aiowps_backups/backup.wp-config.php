<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('WP_CACHE', true);
define( 'WPCACHEHOME', '/vagrant/www/wp-content/plugins/wp-super-cache/' );
define('DB_NAME', 'arshtcannonfund');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         ',.3ao2^yAat] WFb>Kl3Yfb6nuaSjc$p+q+.C`K_5Q9MBZ:v-kDLa]m*e+Sz@pt2');
define('SECURE_AUTH_KEY',  'ulco;=;VC0}1#guiWgov#7R_  )|LY`VQvSF@c5S<@S@zb,i[BcpPHe}s+n{Hum(');
define('LOGGED_IN_KEY',    'X;!NeVdZtve5Nvi.FZ^BX~G$H;a3#yZ@;FdfFdM@o%y/NogFFmzliwV!Dy{?.FPj');
define('NONCE_KEY',        'TSQV}1/!ZBa28|!P4X@W2g`fKLj:<njEq$KlWs??]lZmCgiyo-eRA:$g`cJcr1?K');
define('AUTH_SALT',        '{,$:$qSQs1==m5|YPt& VCYm>ANiKNZA:=uzfW?CP$uj1auDPZ#e5CZ6U#gVq({e');
define('SECURE_AUTH_SALT', '&fv0h5/.[,3rjEq586qiGG7Agk>3RfvT ;$VtZg!^dq~%4b-bV9]~XV0|mc6-XDR');
define('LOGGED_IN_SALT',   '%%AHU+7@;b0*)-5#_/o~Dg66,thne>L+ Sf,in#oZX861BXuHy#zGJ%||U*l-#il');
define('NONCE_SALT',       '<zX{T]-p_J[<KYH(NCPThdAm$%Lbw)|=KE^74[NXT~ix/Q|jnrK<w(<@i90AFK6p');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'gjtus_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
