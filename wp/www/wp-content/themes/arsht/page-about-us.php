<?php /* Template Name: ABOUT-US */ ?>
<?php get_header(); ?>

    <main class="about-page">
        <section class="about-hero">
            <div class="container">
                <h1 class="about-hero-heading"><?php the_field('heading'); ?></h1>
                <div class="about-hero-desc"><?php the_field('text'); ?></div>
            </div>
            <figure class="about-us-image-container"><img class="about-hero-image" src="<?php the_field('featured_image'); ?>"></figure>
        </section> 

        <section class="about-info-blocks">
            <div class="container">
                <?php 
                    $info_blocks = get_field('info_blocks');
                    foreach ($info_blocks as $info_block) { ?>
                    <div class="about-info-block">
                        <h2 class="about-info-block-title"><?php echo $info_block['block_title']; ?></h2>
                        <div class="about-info-block-desc"><?php echo $info_block['block_excerpt']; ?></div>
                        <div class="about-info-block-desc half-left"><?php echo $info_block['block_copy_1']; ?></div>
                        <div class="about-info-block-desc half-right"><?php echo $info_block['block_copy_2']; ?></div>
                    </div>
                    <?php } ?>
            </div>
        </section>
        <section class="about-timeline">
            <div class="container">
                <ul id="about-timeline">
                    <?php 
                    $timeline_items = get_field('chronology');
                    foreach ($timeline_items as $timeline_item) { ?>
                        <li class="about-timeline-item">
                            <div class="about-timeline-item-month"><?php echo $timeline_item['month']; ?></div>
                            <div class="about-timeline-item-year"><?php echo $timeline_item['year']; ?></div>
                            <div class="about-timeline-item-desc"><?php echo $timeline_item['info']; ?></div>
                        </li>
                    <?php } ?>
                </ul>
            </div>
        </section>
    </main>

<?php get_footer(); ?>