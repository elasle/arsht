<?php get_header(); ?>

    <main class="news-page news-single">
        <div class="container">
            <article>
                <header class="news-header">
                    <h1 class="page-title"><?php the_title(); ?></h1>
                    <div class="news-date-author">                            
                        <span class="news-date"><?php $date = get_the_date(); echo $date; ?></span>
                        <span class="news-author"><?php the_author(); ?></span>
                    </div>
                    <div class="share-news">
                        <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>" title="Share on Facebook"><i class="fa fa-facebook"></i></a>
                        <a target="_blank" href="https://twitter.com/home?status=<?php the_permalink(); ?>" title="Share on Twitter"><i class="fa fa-twitter"></i></a>
                        <a target="_blank" href="mailto:?&subject=Arsht Cannon Fund News&body=<?php the_permalink(); ?>" title="Send by Email"><i class="fa fa-envelope"></i></a>
                    </div>
                </header>
                <div class="news-body">
                    <?php the_content(); ?>
                </div>
                <footer class="news-footer">
                    <div class="news-date-author">                            
                        <span class="news-date"><?php echo $date; ?></span>
                        <span class="news-author"><?php the_author(); ?></span>
                    </div>
                    <div class="share-news">
                        <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>" title="Share on Facebook"><i class="fa fa-facebook"></i></a>
                        <a target="_blank" href="https://twitter.com/home?status=<?php the_permalink(); ?>" title="Share on Twitter"><i class="fa fa-twitter"></i></a>
                        <a target="_blank" href="mailto:?&subject=Arsht Cannon Fund News&body=<?php the_permalink(); ?>" title="Send by Email"><i class="fa fa-envelope"></i></a>
                    </div>
                </footer>
            </article>
        </div>
    </main>

<?php get_footer(); ?>