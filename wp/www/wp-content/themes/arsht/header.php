<!doctype html>
<!--[if lt IE 7 ]> <html class="ie ie6 ie-lt10 ie-lt9 ie-lt8 ie-lt7 no-js" lang="en"> <![endif]-->
<!--[if IE 7 ]>    <html class="ie ie7 ie-lt10 ie-lt9 ie-lt8 no-js" lang="en"> <![endif]-->
<!--[if IE 8 ]>    <html class="ie ie8 ie-lt10 ie-lt9 no-js" lang="en"> <![endif]-->
<!--[if IE 9 ]>    <html class="ie ie9 ie-lt10 no-js" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--><html class="no-js" lang="en"><!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title><?php wp_title(); ?></title>
    <?php wp_head(); ?>

    <link rel="shortcut icon" href="<?php echo THEME_FOLDER; ?>/favicon.ico" />
    <meta name="author" content="" />
    <meta name="description" content="" />

    <meta name="google-site-verification" content="" />
    <meta name="Copyright" content="" />

    <link rel="stylesheet" href="https://use.typekit.net/amg6owf.css">
    <!-- <link rel="stylesheet" href="<?php echo THEME_FOLDER; ?>/styles/styles.css" /> -->
    <link rel="stylesheet" href="http://localhost:8081/styles/styles.css" />
    

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="scripts/html5.js"></script>
<![endif]-->
</head>

<body>

<?php
    $Menu_Class = 'black-in-white'; 
    $Menu_Class = is_page('home')                    ? 'white-in-none'              : $Menu_Class;
    $Menu_Class = is_page('about')                   ? 'white-in-red'               : $Menu_Class; 
    $Menu_Class = is_page('grants')                  ? 'black-in-white'             : $Menu_Class; 
?>

    <header id="header" class="<?php echo $Menu_Class;?>" data-spy="affix" data-offset-top="5">
        <h1 class="logo">
            <a href="<?php echo BASE_URL; ?>"><img src="<?php echo THEME_FOLDER; ?>/images/acf-logo-red-back.png" alt="Logo"></a>
        </h1>
        <a class="menu-btn"><div><span></span></div></a>
        <nav class="main-menu">
            <ul>
                <li><a href="<?php echo BASE_URL; ?>" title="Home">Home</a></li>
                <li><a href="<?php echo BASE_URL; ?>/about/" title="About">About</a></li>
                <li><a href="<?php echo BASE_URL; ?>/grants/" title="Grants">Grants</a></li>
                <li><a href="<?php echo BASE_URL; ?>/news/" title="News">News</a></li>
                <li><a href="<?php echo BASE_URL; ?>/contact/" title="Contact">Contact</a></li>
            </ul>
        </nav>
    </header>
