<?php /* Template Name: CONTACT */ ?>
<?php get_header(); ?>


<main class="contact-page">
  <div class="container">
    <h1 class="page-title">Contact</h1>
    <section class="contact-info">
      <figure class="contact-image"><?php the_post_thumbnail(); ?></figure>
      <?php the_content(); ?>
      <button class="contact-bio-cta" data-toggle="modal" data-target="#BioModal">Bio</button>
    </section>
    <section class="contact-form form-container">
      <?php the_field('form'); ?>
   </section>
 </div>
</main>

<!-- Modal -->
<div class="modal fade" id="BioModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="full-name" id="myModalLabel"><?php the_field('full_name'); ?></h4>
      </div>
      <div class="modal-body">
        <?php the_field('bio'); ?>
      </div>
    </div>
  </div>
</div>

<?php get_footer(); ?>