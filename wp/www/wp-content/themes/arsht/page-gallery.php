<?php /* Template Name: GALLERIES */ ?>

<?php get_header(); ?>

    <main class="galleries-page">
        <section class="galleries-list">
            <div class="container">
                <h1 class="page-title">Galleries</h1>
                <section class="galleries-container">
                    <?php
                    $args = array(
                        'post_type'     => 'galleries',
                        'orderby'       => 'ID',
                        'order'         => 'ASC',
                        'posts_per_page'=> 9,
                    );
                    query_posts( $args );
                    if (have_posts()) : ?>
                    <?php while (have_posts()) : the_post(); ?>
                        <div class="gallery-card">
                            <a class="gallery-url" href="<?php the_permalink(); ?> ">
                                <figure><?php the_post_thumbnail(); ?></figure>
                                <h2 class="gallery-title"><?php the_title(); ?></h2>
                            </a>
                        </div>
                    <?php endwhile; ?>
                    <?php endif; wp_reset_query(); ?>
                </section>
            </div>
        </section>
    </main>

<?php get_footer(); ?>