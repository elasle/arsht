<?php get_header(); ?>
    <main class="program-page">
        <section class="program-info">
            <div class="container">
                <h1 class="page-title"><?php the_title(); ?></h1>
                <div class="program-subheading"><?php the_field('heading_quote'); ?></div>
                <div class="program-description">
                    <?php the_content(); ?>
                </div>
            </div>
        </section>
        <section class="program-partners">
            <div class="container">
                <h1 class="program-partners-heading"><?php the_field('heading');?></h1>
                <?php $partners_sections = get_field('partners_sections'); ?>
                <?php foreach ($partners_sections as $partners_section) { ?>
	                <div class="program-partners-subheading"><?php echo $partners_section['partners_description']; ?></div>
	                <ul class="partners-list">
		            <?php
		            $args = array(
		                'post_type'     => 'partners',
		                'orderby'       => 'ID',
		                'order'         => 'ASC',
		                'posts__in'		=> $partners_section['partners'],
		            );
		            query_posts( $args );
		            if (have_posts()) : ?>
		            <?php while (have_posts()) : the_post(); ?>
	                    <li class="partner-card">
	                        <div class="partner-card-content">
	                            <h2 class="partner-name"><?php the_title(); ?></h2>
	                            <div class="partner-desc"><?php echo limitword(get_the_content(), 20); ?></div>
	                            <a class="learn-more" href="<?php the_field('partner_url'); ?>" target="_blank">Learn More</a>
	                        </div>
	                    </li>
		            <?php endwhile; ?>
		        	<?php endif; wp_reset_query(); ?>
			        </ul>
                <?php } ?>
            </div>
        </section>
    </main>
<?php get_footer(); ?>