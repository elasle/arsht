<?php
error_reporting(0);
define('THEME_FOLDER', ''.get_template_directory_uri().'');
define('BASE_URL', get_bloginfo( 'url' ));

remove_action('wp_head', 'wp_generator');
add_filter( 'jpeg_quality', 'jpeg_full_quality' );
function jpeg_full_quality( $quality ) { return 100; }
add_theme_support('post-thumbnails');

/*
 * Custom Post Types
 */

// Model definition
function register_cpt_programs() {

	$labels = array(
		'name'               => _x( 'Programs', 'arsht' ),
		'singular_name'      => _x( 'Program', 'arsht' ),
		'add_new'            => _x( 'Add new', 'arsht' ),
		'add_new_item'       => _x( 'Add new program', 'arsht' ),
		'edit_item'          => _x( 'Edit', 'arsht' ),
		'new_item'           => _x( 'New', 'arsht' ),
		'view_item'          => _x( 'View', 'arsht' ),
		'search_items'       => _x( 'Search', 'arsht' ),
		'not_found'          => _x( 'No programs found', 'arsht' ),
		'not_found_in_trash' => _x( 'No programs in the trash', 'arsht' ),
		'menu_name'          => _x( 'Programs', 'arsht' ),
	);

	$args = array(
		'labels'              => $labels,
		'hierarchical'        => false,
		'description'         => "Program",
		'supports'            => array( 'title','excerpt','editor','thumbnail', 'tags'),
		'taxonomies'          => array(),
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'menu_position'       => 5,
		'show_in_nav_menus'   => true,
		'publicly_queryable'  => true,
		'exclude_from_search' => false,
		'has_archive'         => true,
		'query_var'           => true,
		'can_export'          => true,
		'rewrite'             => true,
		'menu_icon'           => 'dashicons-admin-post',
		'capability_type'     => 'post'
	);

	register_post_type( 'programs', $args );
}

function register_cpt_galleries() {

	$labels = array(
		'name'               => _x( 'Galleries', 'arsht' ),
		'singular_name'      => _x( 'Gallery', 'arsht' ),
		'add_new'            => _x( 'Add new', 'arsht' ),
		'add_new_item'       => _x( 'Add new gallery', 'arsht' ),
		'edit_item'          => _x( 'Edit', 'arsht' ),
		'new_item'           => _x( 'New', 'arsht' ),
		'view_item'          => _x( 'View', 'arsht' ),
		'search_items'       => _x( 'Search', 'arsht' ),
		'not_found'          => _x( 'No galleries found', 'arsht' ),
		'not_found_in_trash' => _x( 'No galleries in the trash', 'arsht' ),
		'menu_name'          => _x( 'Galleries', 'arsht' ),
	);

	$args = array(
		'labels'              => $labels,
		'hierarchical'        => false,
		'description'         => "Gallery",
		'supports'            => array( 'title','editor','thumbnail', 'tags'),
		'taxonomies'          => array(),
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'menu_position'       => 5,
		'show_in_nav_menus'   => true,
		'publicly_queryable'  => true,
		'exclude_from_search' => false,
		'has_archive'         => true,
		'query_var'           => true,
		'can_export'          => true,
		'rewrite'             => true,
		'menu_icon'           => 'dashicons-admin-media',
		'capability_type'     => 'post'
	);

	register_post_type( 'galleries', $args );
}

function register_cpt_partners() {

	$labels = array(
		'name'               => _x( 'Partners', 'arsht' ),
		'singular_name'      => _x( 'Partner', 'arsht' ),
		'add_new'            => _x( 'Add new', 'arsht' ),
		'add_new_item'       => _x( 'Add new partner', 'arsht' ),
		'edit_item'          => _x( 'Edit', 'arsht' ),
		'new_item'           => _x( 'New', 'arsht' ),
		'view_item'          => _x( 'View', 'arsht' ),
		'search_items'       => _x( 'Search', 'arsht' ),
		'not_found'          => _x( 'No partners found', 'arsht' ),
		'not_found_in_trash' => _x( 'No partners in the trash', 'arsht' ),
		'menu_name'          => _x( 'Partners', 'arsht' ),
	);

	$args = array(
		'labels'              => $labels,
		'hierarchical'        => false,
		'description'         => "Partner",
		'supports'            => array( 'title', 'excerpt','editor','thumbnail', 'tags'),
		'taxonomies'          => array(),
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'menu_position'       => 5,
		'show_in_nav_menus'   => true,
		'publicly_queryable'  => true,
		'exclude_from_search' => false,
		'has_archive'         => true,
		'query_var'           => true,
		'can_export'          => true,
		'rewrite'             => true,
		'menu_icon'           => 'dashicons-admin-post',
		'capability_type'     => 'post'
	);

	register_post_type( 'partners', $args );
}

add_action( 'init', 'register_cpt_programs' );
add_action( 'init', 'register_cpt_galleries' );
add_action( 'init', 'register_cpt_partners' );

/**
 * Transform Post into News
 */

function arsht_change_post_label() {
	global $menu;
	global $submenu;
	$menu[5][0] = 'News';
	$submenu['edit.php'][5][0] = 'News';
	$submenu['edit.php'][10][0] = 'Add News';
	$submenu['edit.php'][16][0] = 'News Tags';
}
function arsht_change_post_object() {
	global $wp_post_types;
	$labels = &$wp_post_types['post']->labels;
	$labels->name = 'News';
	$labels->singular_name = 'News';
	$labels->add_new = 'Add News';
	$labels->add_new_item = 'Add News';
	$labels->edit_item = 'Edit News';
	$labels->new_item = 'News';
	$labels->view_item = 'View News';
	$labels->search_items = 'Search News';
	$labels->not_found = 'No News found';
	$labels->not_found_in_trash = 'No News found in Trash';
	$labels->all_items = 'All News';
	$labels->menu_name = 'News';
	$labels->name_admin_bar = 'News';
}

add_action( 'admin_menu', 'arsht_change_post_label' );
add_action( 'init', 'arsht_change_post_object' );

/**
 * Options Page
 */

if( function_exists('acf_add_options_page') ) {

	acf_add_options_page(array(
		'page_title' 	=> 'Theme General Settings',
		'menu_title'	=> 'Theme Settings',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));

	acf_add_options_sub_page(array(
		'page_title' 	=> 'Theme Header Settings',
		'menu_title'	=> 'Header',
		'parent_slug'	=> 'theme-general-settings',
	));

	acf_add_options_sub_page(array(
		'page_title' 	=> 'Theme Footer Settings',
		'menu_title'	=> 'Footer',
		'parent_slug'	=> 'theme-general-settings',
	));

}

function limitword( $str, $num ) {
    $palabras = preg_split( '/[\s]+/', $str, -1, PREG_SPLIT_OFFSET_CAPTURE );
    if( isset($palabras[$num][1]) ){
        $str = trim(substr( $str, 0, $palabras[$num][1] )).'...';
    }
    unset( $palabras, $num );
    return trim( $str );
} 


function get_excerpt(){
$excerpt = get_the_content();
$excerpt = preg_replace(" ([.*?])",'',$excerpt);
$excerpt = strip_shortcodes($excerpt);
$excerpt = strip_tags($excerpt);
$excerpt = substr($excerpt, 0, 180);
$excerpt = substr($excerpt, 0, strripos($excerpt, " "));
$excerpt = trim(preg_replace( '/s+/', ' ', $excerpt));
$excerpt = $excerpt.'...';
return $excerpt;
}

add_filter('max_srcset_image_width', create_function('', 'return 1;'));

function remove_image_size_attributes( $html ) {
    return preg_replace( '/(width|height)="\d*"/', '', $html );
}
 
// Remove image size attributes from post thumbnails
add_filter( 'post_thumbnail_html', 'remove_image_size_attributes' );
 
// Remove image size attributes from images added to a WordPress post
add_filter( 'image_send_to_editor', 'remove_image_size_attributes' );
