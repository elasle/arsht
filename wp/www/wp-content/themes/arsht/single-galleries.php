<?php get_header(); ?>
<main class="galleries-page single-gallery">
    <div class="container">

        <h1 class="page-title"><?php the_title(); ?></h1>
        <a href="#" class="btn-fullscreen" value="click to toggle fullscreen" onclick="toggleFullScreen()"><i class="fa fa-arrows-alt-v"></i><i class="fa fa-expand"></i></a>

		<?php $images = get_field('images'); ?>
		<?php if( $images ): ?>
            <ul id="single-gallery">
				<?php foreach( $images as $image ): ?>
                    <li><img src="<?php echo $image['url']; ?>" /></li>
				<?php endforeach; ?>
            </ul>
		<?php endif; ?>
    </div>

	<?php if( $images ){ ?>
        <div class="gallery-pagers">
            <div id="single-gallery-pager">
				<?php $i = 0; foreach( $images as $image ): ?>
                    <a data-slide-index="<?php echo $i; ?>" href=""><img src="<?php echo $image['sizes']['thumbnail']; ?>" /></a>
					<?php $i++;  endforeach; ?>
            </div>
        </div>
	<?php } ?>
</main>
<?php get_footer(); ?>
