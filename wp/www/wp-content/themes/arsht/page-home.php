<?php /* Template Name: HOME */ ?>
<?php get_header(); ?>
<main class="home-page">
    <section class="home-hero" style="background-image: url('<?php the_field('background_image'); ?>');">
        <div class="container">
            <div class="hero-content">
                <div class="hero-content-info">
                    <h2 class="hero-title"><?php the_field('heading');?></h2>
                    <p class="hero-desc"><?php the_field('subheading'); ?></p>
                    <div class="hero-csta">
                        <a class="hero-cta" href="<?php the_field('learn_more'); ?>" title="Learn more">Learn more</a>
                        <a class="hero-cta" href="<?php the_field('apply'); ?>" title="Apply"><span>Apply</span> <i class="fa fa-chevron-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="home-programs">
        <div class="container">
            <h1 class="section-title">Our Programs</h1>
            <?php
            $args = array(
                'post_type'     => 'programs',
                'orderby'       => 'ID',
                'order'         => 'ASC'
            );
            query_posts( $args );
            if (have_posts()) : ?>
            <?php while (have_posts()) : the_post(); ?>
                <div class="program">
                    <?php the_post_thumbnail(); ?>
                    <h2 class="program-title"><?php the_title(); ?></h2>
                    <p class="program-desc"><?php the_excerpt(); ?></p>
                    <a class="program-url" href="<?php the_permalink();?>" title="More Info">More Info</a>
                </div>
            <?php endwhile; ?>
        <?php endif; wp_reset_query(); ?>
    </div>
</section>

<section class="home-galleries">
    <?php
    $args = array(
        'post_type'     => 'galleries',
        'orderby'       => 'ID',
        'order'         => 'ASC',
        'posts_per_page'=> 3,
    );
    query_posts( $args );
    if (have_posts()) : ?>
    <?php while (have_posts()) : the_post(); ?>
        <div class="gallery-card">
            <a class="gallery-url" href="<?php the_permalink(); ?> ">
                <figure><?php the_post_thumbnail(); ?></figure>
                <h2 class="gallery-title"><?php the_title(); ?></h2>
            </a>
        </div>
    <?php endwhile; ?>
<?php endif; wp_reset_query(); ?>
<a href="<?php the_field('gallery_page_link'); ?>" class="view-galleries"><span>View Galleries</span> <i class="fa fa-chevron-right"></i></a>
</section>

<section class="home-news">
    <div class="container">
        <h1 class="section-title">News &  Noteworthy</h1>
        <div class="featured-news">
            <div class="featured-caption">Featured</div>
            <?php
                $args = array(
                    'post_type'     => 'post',
                    'orderby'       => 'date',
                    'order'         => 'DESC',
                    'posts_per_page'=> 1,
                );
                query_posts( $args );
                if (have_posts()) : ?>
            <?php while (have_posts()) : the_post(); $exclude = $post-> ID; ?>
            <article class="news news-card">
                <header>
                    <h2 class="news-title"><?php the_title(); ?></h2>
                </header>
                <p class="news-body"><?php echo limitword(get_the_excerpt(), 50); ?></p>
                <footer class="news-footer">
                    <div class="news-date-author">                            
                        <span class="news-date"><?php $date = get_the_date(); echo $date; ?></span>
                        <span class="news-author"><?php the_author(); ?></span></div>
                        <a class="read-full-news" href="<?php the_permalink(); ?>">Read More</a>
                    </footer>
                </article>
            <?php endwhile; ?>
        <?php endif; wp_reset_query(); ?>
            </div>
            <div class="news-feed">
                <div class="latest-caption">Latest news</div>
                <?php
                $args = array(
                    'post_type'     => 'post',
                    'orderby'       => 'date',
                    'order'         => 'DESC',
                    'posts_per_page'=> 4,
                    'posts__not_in' => array($exclude),
                );
                query_posts( $args );
                if (have_posts()) : ?>
                <?php while (have_posts()) : the_post(); ?>
                <article class="news">
                    <header>
                        <a class="read-full-news" href="<?php the_permalink(); ?>"><h2 class="news-title"><?php the_title(); ?></h2></a>
                    </header>
                    <footer class="news-footer">
                        <span class="news-date"><?php $date = get_the_date(); echo $date;  ?></span>
                        <span class="news-author"><?php the_author(); ?></span>
                    </footer>
                </article>
            <?php endwhile; ?>
        <?php endif; wp_reset_query(); ?>
        <a class="read-more-news" href="<?php the_field('more_news_link'); ?>"><span>More News</span> <i class="fa fa-chevron-right"></i></a>
    </div>
</div>
</section>
</main>
<?php get_footer(); ?>