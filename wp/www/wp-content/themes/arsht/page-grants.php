<?php /* Template Name: GRANTS */ ?>

<?php get_header(); ?>

    <main class="grants-page">
        <div class="container">
            <h1 class="grants-heading"><?php the_field('heading'); ?></h1>
            <section class="grants-qualifications">


                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

                  <?php $info_blocks = get_field('info_block'); $i = 0;?>
                  <?php foreach ($info_blocks as $info_block) { $i++; ?>

                  <div class="panel">
                    <div class="panel-heading" role="tab" id="heading<?php echo $i; ?>">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $i; ?>" aria-expanded="false" aria-controls="collapse<?php echo $i; ?>">
                          <?php echo $info_block['title']; ?>
                        </a>
                    </div>
                    <div id="collapse<?php echo $i; ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading<?php echo $i; ?>">
                      <div class="panel-body">
                        <?php echo $info_block['content']; ?>
                      </div>
                    </div>
                  </div>
                  <?php } ?>
                </div>
            </section>
            <section class="grants-form form-container">
                <h1 class="form-title"><?php the_field('form_heading'); ?></h1>
                <p class="form-caption"><?php the_field('form_subheading');?></p>
                <?php the_field('form'); ?>
            </section>
        </div>
    </main>


<?php get_footer(); ?>