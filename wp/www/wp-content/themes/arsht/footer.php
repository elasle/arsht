    <footer id="footer">
        <div class="logo">
            <img src="<?php echo THEME_FOLDER; ?>/images/acf-logo-white-back.png" alt="Logo">
        </div>
        <nav class="footer-menu">
            <ul>
                <li><a href="<?php echo BASE_URL; ?>" title="Home">Home</a></li>
                <li><a href="<?php echo BASE_URL; ?>/about/" title="About">About</a></li>
                <li><a href="<?php echo BASE_URL; ?>/grants/" title="Grants">Grants</a></li>
                <li><a href="<?php echo BASE_URL; ?>/news/" title="News">News</a></li>
                <li><a href="<?php echo BASE_URL; ?>/contact/" title="Contact">Contact</a></li>
            </ul>
        </nav>
    </footer>
</section>

<!-- <script type="text/javascript" src="<?php echo THEME_FOLDER; ?>/scripts/libraries.js"></script> -->
<!-- <script type="text/javascript" src="<?php echo THEME_FOLDER; ?>/scripts/scripts.js"></script> -->

<script type="text/javascript" src="http://localhost:8081/scripts/libraries.js"></script>
<script type="text/javascript" src="http://localhost:8081/scripts/scripts.js"></script>

<script type="text/javascript">
    $(document).ready(function () {
        initPage();
    });
</script>

<?php wp_footer(); ?>
</body>
</html>