var initPage = function() {
    "use strict";
    ARHST.init();
};

$(document).ready(function() {
	$('.menu-btn').on('click', function() {
		$(this).toggleClass('active')
		$('.main-menu').toggleClass('active')
	});
})

var ARHST = {
	init: function() {
		ARHST.handlePage();
	},
	handlePage: function() {
		if ($('main').hasClass('about-page')) {
			pageAbout.init();
		}
		if ($('main').hasClass('single-gallery')) {
			singleGallery.init();
		}
	}
}

var pageAbout = {
	originalSlider: false,
	init: function() {
		pageAbout.originalSlider = $('#about-timeline').bxSlider({
    		captions: true,
    		slideWidth: 5000,
    		pager: false,
    		minSlides: 3,
    		maxSlides: 3,
    		slideMargin: 20,
		});
		pageAbout.reloadSlider();
		$(window).on('resize', function() {
			pageAbout.reloadSlider();
		})
	},
	reloadSlider: function() {
		if ($(window).width() > 992) {
			pageAbout.originalSlider.reloadSlider({
	    		captions: true,
	    		slideWidth: 5000,
	    		pager: false,
	    		minSlides: 3,
	    		maxSlides: 3,
	    		slideMargin: 20,
			});
		}
		else if ($(window).width() > 768) {
			pageAbout.originalSlider.reloadSlider({
	    		captions: true,
	    		slideWidth: 5000,
	    		pager: false,
	    		minSlides: 2,
	    		maxSlides: 2,
	    		slideMargin: 20,
			});
		}
		else {
			pageAbout.originalSlider.reloadSlider({
	    		captions: true,
	    		slideWidth: 5000,
	    		pager: false,
	    		minSlides: 1,
	    		maxSlides: 1,
	    		slideMargin: 20,
			});
		}
	}
}

var singleGallery = {
	originalSlider: false,
	originalSliderPager: false,
	init: function() {

		singleGallery.originalSlider = $('#single-gallery').bxSlider({
			pagerCustom: '#single-gallery-pager',
			auto:false,
            mode: "fade",
			slideWidth: 5000,
			infiniteLoop:false,
			minSlides: 1,
			maxSlides: 1,
			slideMargin: 0,
		});

		singleGallery.originalSliderPager = $('#single-gallery-pager').bxSlider({
    		captions: true,
    		slideWidth: 5000,
			auto:false,
    		pager: false,
			infiniteLoop:false,
    		minSlides: 12,
    		maxSlides: 12,
    		slideMargin: 20,
		});
		singleGallery.reloadSlider();
		$(window).on('resize', function() {
			singleGallery.reloadSlider();
		})
	},
	reloadSlider: function() {
		if ($(window).width() > 992) {
			singleGallery.originalSliderPager.reloadSlider({
	    		captions: true,
	    		slideWidth: 5000,
				auto:false,
				infiniteLoop:false,
	    		pager: false,
	    		minSlides: 12,
	    		maxSlides: 12,
	    		slideMargin: 10,
			});
		}
		else if ($(window).width() > 768) {
			singleGallery.originalSliderPager.reloadSlider({
	    		captions: true,
	    		slideWidth: 5000,
				infiniteLoop:false,
				auto:false,
	    		pager: false,
	    		minSlides: 6,
	    		maxSlides: 6,
	    		slideMargin: 10,
			});
		}
		else {
			singleGallery.originalSliderPager.reloadSlider({
	    		captions: true,
	    		slideWidth: 5000,
				infiniteLoop:false,
				auto:false,
	    		pager: false,
	    		minSlides: 4,
	    		maxSlides: 4,
	    		slideMargin: 10,
			});
		}
	}
}

  function toggleFullScreen() {
  if ((document.fullScreenElement && document.fullScreenElement !== null) ||    
   (!document.mozFullScreen && !document.webkitIsFullScreen)) {
    if (document.documentElement.requestFullScreen) {  
      document.documentElement.requestFullScreen();  
    } else if (document.documentElement.mozRequestFullScreen) {  
      document.documentElement.mozRequestFullScreen();  
    } else if (document.documentElement.webkitRequestFullScreen) {  
      document.documentElement.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);  
    }  
  } else {  
    if (document.cancelFullScreen) {  
      document.cancelFullScreen();  
    } else if (document.mozCancelFullScreen) {  
      document.mozCancelFullScreen();  
    } else if (document.webkitCancelFullScreen) {  
      document.webkitCancelFullScreen();  
    }  
  }  
}