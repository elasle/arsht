<?php /* Template Name: NEWS */ 

$paginating = false;
if (($_GET['t'] == MD5($_GET['ajax'].'3st03sunp4ss')) && ($_GET['pageno'] > 1)) {
    $paginating = true;
}
if (!$paginating) {

?>
<?php get_header(); ?>

    <main class="news-page">
        <section class="news-header">
            <div class="container">
                <h1 class="news-heading">News and Noteworthy</h1>
                <form action="' . home_url( '/' ) . '" method="get" accept-charset="utf-8" class="search-news">
                    <input type="text" name="s" placeholder="Search for Past Articles">
                    <button type="submit" name=""></button>
                </form>
            </div>
        </section>
        <section class="news-feed">
            <div class="container">

                <?php
                    $args = array(
                        'post_type'         => 'post',
                        'orderby'           => 'date',
                        'order'             => 'DESC',
                        's'                 => $_GET['s']
                        'posts_per_page'    => 4,
                    );
                    query_posts( $args );
            
                    global $wp_query;
                    $total = $wp_query->found_posts;
                   
                    if (have_posts()) : ?>
                    <h3 class="featured-caption"><?php echo $total ?> Result<?php if ($total > 1) echo 's';?></h3>
                    <div class="featured-news">
                    <?php while (have_posts()) : the_post(); $exclude[] = $post-> ID; ?>
                        <article class="news-card">
                            <header>
                                <a href="<?php the_permalink(); ?>"><h2 class="news-title"><?php the_title(); ?></h2></a>
                            </header>
                            <div class="news-body"><?php echo limitword(get_the_excerpt(), 30); ?></div>
                            <footer class="news-footer">
                                <div class="news-date-author">                            
                                    <span class="news-date"><?php $date = get_the_date(); echo $date; ?></span>
                                    <span class="news-author"><?php the_author(); ?></span></div>
                            </footer>
                        </article>
                    <?php endwhile; ?>
                <?php endif; ?>
                </div>
            </div>
            <a href="#" class="load-more-news">Load More</a>
        </section>
    </main>


            <?php if ($total <= get_option('posts_per_page')) { ?>

            <?php } else { ?>
            <script type="text/javascript">

                jQuery(document).ready(function() {
                });
                jQuery(window).load(function() {
                    SED.init();
                });

                var mmOpenened = false;
                var tmOpenened = false;
                var paginateFinito = false;
                var WaitPaginating = false;
                var page = 1;
                var uri;
                var uri;
                var lastWidth = window.innerWidth;
                var lastWidthHeight = window.innerWidth;
                var clicked;
                var movingSlide = false;

                var SED = {
                    paginateFinito: false,
                    WaitPaginating: false,
                    init: function () {     
                    },
                    paginateElements: function(aux, t, paginated) {
                        if (SED.WaitPaginating) {   
                            page++;
                            if (page == 2) {
                                $('<div id="loading" class="col-xs-12 text-center"><h4><i class="fa fa-2x fa-spinner fa-spin"></i></h4></div>').insertAfter('.latest-news')
                            }
                    // uri = (uri.indexOf('?') >= 0) ? uri + '&' : uri + '?';
                    // url = uri + '?paged=' + page + '&pageno=' + page + '&ajax='+aux+'&t=' + t,
                    url = uri + 'page/' + page + '/?pageno=' + page + '&ajax='+aux+'&t=' + t;
                    console.log('url');
                    if (uri == '/') url = uri + '?pageno=' + page + '&ajax='+aux+'&t=' + t;
                    if ((uri.indexOf('?') >= 0)) url = 'page/'+page + uri + '&pageno=' + page + '&ajax='+aux+'&t=' + t;
                    $('#loading h4').html('<i class="fa fa-2x fa-spinner fa-spin"></i>');
                    $.get(url,
                        function(data) {
                            if (data != "") {
                                $('.featured-news').append(data);
                                SED.WaitPaginating =  false;
                                $('#loading h4').html('');
                                if (($('.news-card').length < page*paginated) || ($('.news-card').length == totalOfEntries)) {
                                    $('#loading h4').html('');
                                    SED.paginateFinito = true;
                                    $('.load-more-news').remove();

                                }
                            }
                            else {
                                $('#loading h4').html('');
                                SED.paginateFinito = true;
                                $('.load-more-news').remove();
                            }
                        }
                    );
                }
            },
        }



        var aux = <?php $aux = rand(0, 9999); echo $aux; ?>;
        var posts_per_page = 6;
        var t = '<?php echo MD5($aux.'3st03sunp4ss'); ?>';
        var totalOfEntries = '<?php echo $total; ?>';
        uri = '<?php echo $_SERVER['REQUEST_URI']; ?>';

        jQuery(window).load(function() {
            $(window).scroll(function() {
                $('.load-more-news').on('click', function(e) {
                    e.preventDefault();
                    if ((!SED.WaitPaginating) && (!SED.paginateFinito)) {
                        SED.WaitPaginating = true;
                        SED.paginateElements(aux, t, posts_per_page);
                    }
                })
            });
        });
    </script>

    <?php } ?>

<?php wp_reset_query(); ?>


<?php get_footer(); ?>
<?php } else {
    wp_reset_query();

                    $args = array(
                        'post_type'         => 'post',
                        'orderby'           => 'date',
                        'order'             => 'DESC',
                        'posts_per_page'    => 4,
                        'paged'             => 1,
                    );
                    query_posts( $args );
                    if (have_posts()) : 
                        while (have_posts()) : the_post(); 
                            $exclude[] = $post-> ID; 
                        endwhile;
                    endif;wp_reset_query();


    $args = array(
        'post_type'     => 'post',
        'orderby'       => 'date',
        'order'         => 'desc',
        'post_per_page' => 6,
        'paged'         => $_GET['pageno'],
        'post__not_in'  => $exclude,
        );
    query_posts($args);
    while ( have_posts() ) : the_post();?>
        <article class="news-card">
            <header>
                <a href="<?php the_permalink(); ?>"><h2 class="news-title"><?php the_title(); ?></h2></a>
            </header>
            <div class="news-body"><?php echo limitword(get_the_excerpt(), 30); ?></div>
            <footer class="news-footer">
                <div class="news-date-author">                            
                    <span class="news-date"><?php $date = get_the_date(); echo $date; ?></span>
                    <span class="news-author"><?php the_author(); ?></span></div>
            </footer>
        </article>
<?php endwhile; } ?>