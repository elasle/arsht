module.exports = function() {

	var orig = {
		'libraries'			: './libraries/',
		'fontBootstrap'		: './libraries/bootstrap/fonts/**.*',
		'fontAwesome'		: './libraries/font-awesome/fonts/**.*',
		'bxsliderImages'	: './libraries/bxslider-4/src/images/**/**.*',
		'compatibility'		: './assets/scripts/compatibility.js',
		'sourcesImages'		: './assets/images/',
		'faviconImages'		: './assets/images/favicon/**.*',
		'designImages'		: './assets/images/**/**.*',
		'fontStyles'		: './assets/fonts/**.*'
	}

	var dest = {
		'styles'	: '../dist/styles/',
		'scripts'	: '../dist/scripts/',
		'images'	: '../dist/images/',
		'fonts'		: '../dist/fonts/',
		'html'		: '../dist/',
	}

	var order = {
		scripts: [
		'**/general-script.js',
		]
	};

	return {
		orig: orig,
		dest: dest,
		order: order
	};
}