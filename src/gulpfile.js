var gulp				= require('gulp');
var less				= require('gulp-less');
var echo				= require('gulp-print');
var order				= require('gulp-order');
var filter 				= require('gulp-filter');
var concat				= require('gulp-concat');
var uglify				= require('gulp-uglify');
var cleanCss			= require('gulp-clean-css');
var sourcemaps			= require('gulp-sourcemaps');
var imagemin			= require('gulp-imagemin');
var cssUrlAdjuster 		= require('gulp-css-url-adjuster');
var cssBase64 			= require('gulp-css-base64');
var autoprefixer 		= require('gulp-autoprefixer');
var mainBowerFiles 		= require('main-bower-files');
var livereload 			= require('gulp-livereload');
var connect 			= require('gulp-connect');
var config				= require('./gulpconfig.js')();

gulp.task('libraries', function(){

	const html5false = filter(['**/*.js', '!**/html5shiv.js', '!**/respond.src.js'], {restore: true});
	const html5true = filter(['**/html5shiv.js', '**/respond.src.js'], {restore: true});

	gulp.src(
		mainBowerFiles()
	)
	.pipe(html5false)
	.pipe(echo())
	.pipe(sourcemaps.init())
	.pipe(concat('libraries.js'))	
	.pipe(uglify())
	.pipe(sourcemaps.write('./'))
	.pipe(gulp.dest(config.dest.scripts))
	.pipe(html5false.restore)
	.pipe(html5true)
	.pipe(sourcemaps.init())
	.pipe(concat('html5.js'))
	.pipe(uglify())
	.pipe(sourcemaps.write('./'))
	.pipe(gulp.dest(config.dest.scripts))
	.pipe(html5true.restore);
});

gulp.task('scripts', function(){
	gulp.src('./assets/scripts/**/*-script.js')
	.pipe(order(config.order.scripts))
	.pipe(sourcemaps.init())
	.pipe(concat('scripts.js'))	
	.pipe(uglify())
	.pipe(sourcemaps.write('./'))
	.pipe(gulp.dest(config.dest.scripts));
});

gulp.task('common', function(){
	gulp.src(config.orig.fontBootstrap)
	.pipe(gulp.dest(config.dest.fonts));
	gulp.src(config.orig.fontAwesome)
	.pipe(gulp.dest(config.dest.fonts));
	gulp.src(config.orig.fontStyles)
	.pipe(gulp.dest(config.dest.fonts));
	gulp.src(config.orig.designImages)
	.pipe(imagemin())
	.pipe(gulp.dest(config.dest.images));
	gulp.src(config.orig.compatibility)
	.pipe(sourcemaps.init())
	.pipe(uglify())
	.pipe(sourcemaps.write('./'))
	.pipe(gulp.dest(config.dest.scripts));
});

function executeUrlAdjuster(value1,value2) {
	return cssUrlAdjuster({
		replace:  [value1,value2]
	})
}

gulp.task('styles', function(){
	gulp.src('./assets/styles/styles.less')
	.pipe(sourcemaps.init())
	.pipe(less())
	.pipe(executeUrlAdjuster('../fonts','/fonts/'))
	.pipe(cssBase64())
	.pipe(cleanCss({keepSpecialComments:0}))
	.pipe(autoprefixer())
	.pipe(sourcemaps.write('./'))
	.pipe(gulp.dest(config.dest.styles))
	.pipe(livereload());
});

gulp.task('watch', function() {
  livereload.listen();
  gulp.watch('./assets/styles/**/*.less',['styles']);
  gulp.watch('./assets/scripts/**/*.js',['scripts']);
  gulp.watch('./assets/images/**/*.*',['images']);
  gulp.watch('./assets/**/*.html',['html']);
});

gulp.task('connect', function() {
    var cors = function (req, res, next) {
        res.setHeader('Access-Control-Allow-Origin', '*');
        next();
    };

    connect.server(
        {
            root: '../dist/',
            port: 8081,
            livereload: true,
            middleware: function () {
                return [cors];
            }
        }
	);
});

gulp.task('html', function () {
    gulp.src('./assets/**/*.html')
        .pipe(gulp.dest( config.dest.html ));
});


gulp.task('build', ['libraries','scripts','common','styles']);

gulp.task('dev', ['watch', 'connect']);

gulp.task('default', ['build']);