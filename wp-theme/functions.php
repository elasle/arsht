<?php
define('TD', ''.get_template_directory_uri().'');
define('BASE_URL', get_bloginfo( 'url' ));

remove_action('wp_head', 'wp_generator');
add_filter( 'jpeg_quality', 'jpeg_full_quality' );
function jpeg_full_quality( $quality ) { return 100; }
add_theme_support('post-thumbnails');


add_filter( 'xmlrpc_methods', function( $methods ) {
   unset( $methods['pingback.ping'] );
   return $methods;
} );

?>