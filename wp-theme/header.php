<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <title><?php wp_title(); ?></title>
    <?php wp_head(); ?>
    <link rel="shortcut icon" href="<?php echo TD ?>/favicon.ico" />
    <link rel="stylesheet" type="text/css" href="<?php echo TD; ?>/style.css">
</head>